#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>

using namespace std;
using namespace eosio;

class blockban : public eosio::contract {
public:
    blockban(account_name self)
            : contract(self), _accounts(_self, _self), _boards(_self, _self), _tasks(_self, _self) {}

    void transfer(account_name from, account_name to, uint64_t quantity) {
        require_auth(from);

        const auto &fromacnt = _accounts.get(from);
        eosio_assert(fromacnt.balance >= quantity, "overdrawn balance");
        _accounts.modify(fromacnt, from, [&](auto &a) { a.balance -= quantity; });

        add_balance(from, to, quantity);
    }

    void issue(account_name to, uint64_t quantity) {
        require_auth(_self);
        add_balance(_self, to, quantity);
    }

    void cboard(account_name owner, string name) {
        require_auth(owner);

        uint64_t count = 1;
        for (auto itr = _boards.begin(); itr != _boards.end(); ++itr) {
            ++count;
        }

        print(count);

        _boards.emplace(owner, [&](auto &b) {
            b.id = count;
            b.owner = owner;
            b.name = name;
        });


    }

    void ctask(account_name owner, string name, string description, uint64_t board, uint64_t cost) {
        require_auth(owner);
        _boards.get(board); // Проверяем что доска создана
        const auto &account = _accounts.get(owner);

        eosio_assert(account.lock + cost < account.balance, "insufficient funds");

        uint64_t count = 1;
        for (auto itr = _tasks.begin(); itr != _tasks.end(); ++itr) {
            ++count;
        }
        // Создаем таску
        //
        _tasks.emplace(owner, [&](auto &t) {
            t.id = count;
            t.description = description;
            t.name = name;
            t.assignee = owner;
            t.owner = owner;
            t.cost = cost;
            t.board_id = board;
        });

        print(count);

        // Удерживаем с баланса заявителя по таске число токенов равное стоимости таски (гарантия)
        //
        add_lock(owner, owner, cost);
    }

    void accept(account_name user, uint64_t task) {
        require_auth(user);

        const auto &taskref = _tasks.get(task);
        const auto &boardref = _boards.get(taskref.board_id);

        eosio_assert(taskref.status == 0, "should be in todo");
        eosio_assert(taskref.owner != user, "should not be owner");
        eosio_assert(std::find(boardref.users.begin(), boardref.users.end(), user) != boardref.users.end(), "should be possible execitor");

        _tasks.modify(taskref, user, [&](auto &t) {
            t.assignee = user;
            t.actual_executor = user;
            t.status = 1;
        });
    }

    void progress(account_name owner, uint64_t task) {
        require_auth(owner);

        const auto &taskref = _tasks.get(task);

        eosio_assert(taskref.owner == owner, "should be owner");
        eosio_assert(taskref.status == 1, "should be in accepted");

        _tasks.modify(taskref, owner, [&](auto &t) {
            t.status = 2;
        });
    }

    void finish(account_name user, uint64_t task) {
        require_auth(user);

        const auto &taskref = _tasks.get(task);

        eosio_assert(taskref.actual_executor == user, "should be actual executor");
        eosio_assert(taskref.status == 2 || taskref.status == 5, "should be in progress");

        _tasks.modify(taskref, user, [&](auto &t) {
            t.assignee = t.owner;
            t.status = 3;
        });
    }

    void confirm(account_name owner, uint64_t task) {
        require_auth(owner);

        const auto &taskref = _tasks.get(task);

        eosio_assert(taskref.owner == owner, "should be owner");
        eosio_assert(taskref.status == 3, "should be finished");

        _tasks.modify(taskref, owner, [&](auto &t) {
            t.assignee = t.owner;
            t.status = 4;
        });

        // Переводим заблокированные средства исполнителю
        //
        const auto &fromacnt = _accounts.get(owner);
        _accounts.modify(fromacnt, owner, [&](auto &a) {
            a.lock -= taskref.cost;
        });

        _accounts.modify(fromacnt, owner, [&](auto &a) { a.balance -= taskref.cost; });

        add_balance(owner, taskref.actual_executor, taskref.cost);
    }

    void reject(account_name owner, uint64_t task) {
        require_auth(owner);

        const auto &taskref = _tasks.get(task);

        eosio_assert(taskref.owner == owner, "should be owner");
        eosio_assert(taskref.status == 3, "should be finished");

        _tasks.modify(taskref, owner, [&](auto &t) {
            t.assignee = t.actual_executor;
            t.status = 5;
        });
    }

    void arbitrage(account_name user, uint64_t task) {
        require_auth(user);

        const auto &taskref = _tasks.get(task);
        eosio_assert(taskref.status == 5, "should be finished");

        _tasks.modify(taskref, user, [&](auto &t) {
            t.status = 6;
        });
    }

    void addarbiter(account_name arbiter, uint64_t task) {
        require_auth(arbiter);

        const auto &taskref = _tasks.get(task);

        eosio_assert(taskref.status == 6, "should be in abitrage");
        eosio_assert(taskref.owner != arbiter, "arbiter can not be task owner");
        eosio_assert(taskref.actual_executor != arbiter, "arbiter can not be task executor");


        _tasks.modify(taskref, arbiter, [&](auto &t) {
            t.arbiter = arbiter;
            t.status = 7;
        });
    }

    void resolve(account_name arbiter, uint64_t task, bool desision, string comment) {
        require_auth(arbiter);



        const auto &taskref = _tasks.get(task);

        eosio_assert(taskref.status == 7, "should be in abitrage");
        eosio_assert(taskref.owner != arbiter, "arbiter can not be task owner");
        eosio_assert(taskref.actual_executor != arbiter, "arbiter can not be task executor");
        eosio_assert(taskref.arbiter == arbiter, "arbiter can not be task owner");

        const auto sum = taskref.cost / 20;
        const auto payment = taskref.cost - sum;

        print(sum);
        print(" ");
        print(payment);

        _tasks.modify(taskref, arbiter, [&](auto &t) {
            t.status = 8;
            t.desision = comment;
        });

        const auto owner = taskref.owner;


        // Разблокируем средства на счете инициатора задачи
        //
        const auto &fromacnt = _accounts.get(owner);
        _accounts.modify(fromacnt, owner, [&](auto &a) {
            a.lock -= taskref.cost;
        });


        if (desision) {
            // Решение в пользу исполнителя - перечисляем деньги испоонитеою
            //
            _accounts.modify(fromacnt, owner, [&](auto &a) { a.balance -= payment ; });
            add_balance(owner, taskref.actual_executor, payment);
        }

        // В любом случае переводим 5% арбитру
        //
        _accounts.modify(fromacnt, owner, [&](auto &a) { a.balance -= sum; });
        add_balance(owner, taskref.arbiter, sum);
    }

    void mkarbitr(account_name user, bool arbiter) {
        require_auth(user);

        makearbiter(user, arbiter);
    }

    void auserboard(account_name owner, account_name user, uint64_t board) {
        require_auth(owner);

        const auto &boardref = _boards.get(board);
        eosio_assert(boardref.owner == owner, "should be owner");


        _boards.modify(boardref, owner, [&](auto &b) {
            b.users.push_back(user);
        });
    }

private:

    //@abi table boards2
    struct board {
        uint64_t id;
        account_name owner;
        vector<account_name> users;
        string name;


        uint64_t primary_key() const { return id; }
    };

    //@abi table tasks2
    struct task {
        uint64_t id;
        uint64_t board_id;
        uint8_t status = 0;
        string name;
        string description;
        account_name owner;
        account_name actual_executor;
        account_name assignee;
        uint64_t cost;
        account_name arbiter;
        string desision;

        uint64_t primary_key() const { return id; }
    };

    //@abi table accounts2
    struct account {
        account_name owner;
        uint64_t balance;
        uint64_t lock;
        bool arbiter;

        uint64_t primary_key() const { return owner; }
    };

    eosio::multi_index<N(accounts2), account> _accounts;

    eosio::multi_index<N(boards2), board> _boards;

    eosio::multi_index<N(tasks2), task> _tasks;

    void add_balance(account_name payer, account_name to, uint64_t q) {
        auto toitr = _accounts.find(to);
        if (toitr == _accounts.end()) {
            _accounts.emplace(payer, [&](auto &a) {
                a.owner = to;
                a.balance = q;
            });
        } else {
            _accounts.modify(toitr, 0, [&](auto &a) {
                a.balance += q;
                eosio_assert(a.balance >= q, "overflow detected");
            });
        }
    }

    void add_lock(account_name payer, account_name to, uint64_t q) {
        auto toitr = _accounts.find(to);
        if (toitr == _accounts.end()) {
            _accounts.emplace(payer, [&](auto &a) {
                a.owner = to;
                a.lock = q;
            });
        } else {
            _accounts.modify(toitr, 0, [&](auto &a) {
                a.lock += q;
                eosio_assert(a.lock >= q, "overflow detected");
            });
        }
    }

    void makearbiter(account_name user, bool arbiter) {
        auto toitr = _accounts.find(user);
        if (toitr == _accounts.end()) {
            _accounts.emplace(user, [&](auto &a) {
                a.arbiter = arbiter;
            });
        } else {
            _accounts.modify(toitr, 0, [&](auto &a) {
                a.arbiter = arbiter;
            });
        }
    }


};

EOSIO_ABI( blockban, (transfer)(issue)(cboard)(ctask)(accept)(progress)(finish)(confirm)(reject)(mkarbitr)(arbitrage)(addarbiter)(resolve)(auserboard)
)